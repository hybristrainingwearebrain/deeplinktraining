package com.training.deeplink.service;

import com.training.deeplink.enums.Status;
import com.training.deeplink.model.DeepLinkModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface DeepLinkService {


    /**
     * Gets all deepLinks in the system.
     *
     * @return all deepLinks in the system.
     */
    List<DeepLinkModel> getDeepLinks();


    /**
     * Gets all deepLinks of the product.
     *
     * @param productModel product which deepLinks will be fined.
     * @return all deepLinks related to the product.
     */
    List<DeepLinkModel> getDeepLinksByProduct(ProductModel productModel);

    List<DeepLinkModel> getDeepLinksByStatus(Status status);

    List<DeepLinkModel> getVerifiedDeepLinksByProduct(ProductModel productModel);

    void saveDeepLink(DeepLinkModel deepLink);
}
