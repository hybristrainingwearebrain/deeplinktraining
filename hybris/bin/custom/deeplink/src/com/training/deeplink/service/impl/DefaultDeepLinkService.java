package com.training.deeplink.service.impl;

import com.training.deeplink.daos.DeepLinkDAO;
import com.training.deeplink.enums.Status;
import com.training.deeplink.model.DeepLinkModel;
import com.training.deeplink.service.DeepLinkService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public class DefaultDeepLinkService implements DeepLinkService {

    private DeepLinkDAO deepLinkDAO;

    /**
     * Gets all deepLinks in the system.
     *
     * @return all deepLinks in the system.
     */
    @Override
    public List<DeepLinkModel> getDeepLinks() {
        return deepLinkDAO.findDeepLinks();
    }

    /**
     * Gets all deepLinks of the product.
     *
     * @param productModel product which deepLinks will be fined.
     * @return all deepLinks related to the product.
     */
    @Override
    public List<DeepLinkModel> getDeepLinksByProduct(ProductModel productModel) {
        return deepLinkDAO.findDeepLinksByProduct(productModel);
    }

    @Override
    public List<DeepLinkModel> getDeepLinksByStatus(Status status) {
        return deepLinkDAO.findDeepLinksByStatus(status);
    }

    @Override
    public List<DeepLinkModel> getVerifiedDeepLinksByProduct(ProductModel productModel) {
        return deepLinkDAO.findDeepLinksByProductAndStatus(productModel, Status.VERIFIED);
    }

    @Override
    public void saveDeepLink(DeepLinkModel deepLink) {
        deepLinkDAO.saveDeepLink(deepLink);
    }

    public DeepLinkDAO getDeepLinkDAO() {
        return deepLinkDAO;
    }

    public void setDeepLinkDAO(DeepLinkDAO deepLinkDAO) {
        this.deepLinkDAO = deepLinkDAO;
    }
}
