package com.training.deeplink.service.impl;

import com.training.deeplink.model.DeepLinkModel;
import com.training.deeplink.service.DeepLinkService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@IntegrationTest
public class DefaultDeepLinkServiceIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private DeepLinkService deepLinkService;

    @Resource
    private ModelService modelService;

    private DeepLinkModel deepLink;
    private final String DEEPLINK_NAME = "LINK_NAME";
    private final String DEEPLINK_URL = "http://link.url";

    private ProductModel product;


    @Before
    public void setUp() {
        CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
        CatalogModel catalogModel = new CatalogModel();
        catalogModel.setId("electronicsProductCatalog");
        catalogVersionModel.setCatalog(catalogModel);
        catalogVersionModel.setVersion("Online");

        product = modelService.create(ProductModel.class);
        deepLink = modelService.create(DeepLinkModel.class);

        product.setDeepLinks(Collections.singletonList(deepLink));
        product.setName("my productname");
        product.setCatalogVersion(catalogVersionModel);
        product.setCode("12345");

        deepLink.setName(DEEPLINK_NAME);
        deepLink.setUrl(DEEPLINK_URL);
        deepLink.setProducts(Collections.singleton(product));    }

    @Test
    public void testDeepLinkService() {
        int size = deepLinkService.getDeepLinks().size();

        modelService.save(product);
        modelService.save(deepLink);

        List<DeepLinkModel> links = deepLinkService.getDeepLinks();

        Assert.assertEquals(size + 1, links.size());

        List<DeepLinkModel> linksByProduct = deepLinkService.getDeepLinksByProduct(product);

        DeepLinkModel fetchedDeeplink = linksByProduct.get(0);

        Assert.assertEquals(DEEPLINK_NAME, fetchedDeeplink.getName());
        Assert.assertEquals(DEEPLINK_URL, fetchedDeeplink.getUrl());
    }
}